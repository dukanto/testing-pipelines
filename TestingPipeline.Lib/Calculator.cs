namespace TestingPipeline.Lib
{
    public class Calculator
    {
        /// <summary>
        /// Performs an arythmetic add on the two passed numbers
        /// </summary>
        /// <param name="num1">First number to add</param>
        /// <param name="num2">Second one</param>
        /// <returns>The arythmetic sum</returns>
        public static int add(int num1, int num2)
        {
            return num1 + num2;
        }

        /// <summary>
        /// Substracts num2 to num1
        /// </summary>
        /// <param name="num1">The number from which it'll substract</param>
        /// <param name="num2">The number to be subsctracted</param>
        /// <returns>num2 - num1</returns>
        public static int substract(int num1, int num2)
        {
            return num1 - num2;
        }

        public static int multiply(int num1, int num2)
        {
            return num1 * num2;
        }

        /// <summary>
        /// Returns the INTEGER division from two numbers
        /// </summary>
        /// <param name="num1">Dividend</param>
        /// <param name="num2">Divisor</param>
        /// <returns>The integer part of the result; i.e. 13 / 5 = 2</returns>
        public static int integerDivision(int num1, int num2)
        {
            return num1 / num2;
        }
    }
}
