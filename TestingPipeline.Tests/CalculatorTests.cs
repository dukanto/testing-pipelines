using NUnit.Framework;
using System.Collections.Generic;
using System.ComponentModel;
using TestingPipeline.Lib;

namespace TestingPipeline.Tests
{
    public class CalculatorTests
    {
        private List<IntValuesResult> _addValues;
        private List<IntValuesResult> _substractValues;
        private List<IntValuesResult> _multiplyValues;
        private List<IntValuesResult> _divideValues;

        [SetUp]
        public void Setup()
        {
            this._addValues = new List<IntValuesResult>();
            this._addValues.Add(new IntValuesResult(1, 1, 2));
            this._addValues.Add(new IntValuesResult(1, 2, 3));
            this._addValues.Add(new IntValuesResult(1, 3, 4));
            this._addValues.Add(new IntValuesResult(5, 0, 5));

            this._substractValues = new List<IntValuesResult>();
            this._substractValues.Add(new IntValuesResult(1, 1, 0));
            this._substractValues.Add(new IntValuesResult(1, 2, -1));
            this._substractValues.Add(new IntValuesResult(100, 0, 100));
            this._substractValues.Add(new IntValuesResult(-1, -1, 0));
            this._substractValues.Add(new IntValuesResult(-1, 1, -2));

            this._multiplyValues = new List<IntValuesResult>();
            this._multiplyValues.Add(new IntValuesResult(1, 1, 1));
            this._multiplyValues.Add(new IntValuesResult(1, 2, 2));
            this._multiplyValues.Add(new IntValuesResult(1, 4, 4));
            this._multiplyValues.Add(new IntValuesResult(5, -1, -5));
            this._multiplyValues.Add(new IntValuesResult(10, 10000, 100000));

            this._divideValues = new List<IntValuesResult>();
            this._divideValues.Add(new IntValuesResult(1, 1, 1));
            this._divideValues.Add(new IntValuesResult(2, 1, 2));
            this._divideValues.Add(new IntValuesResult(2, 2, 1));
            this._divideValues.Add(new IntValuesResult(5, 2, 2));
            this._divideValues.Add(new IntValuesResult(39, 6, 6));
        }

        [Theory]
        public void TestAdd()
        {
            int addResult = 0;
            foreach (IntValuesResult ivr in this._addValues)
            {
                addResult = Calculator.add(ivr.value1, ivr.value2);
                Assert.AreEqual(ivr.result, addResult, "Values does not match!");
            } 
        }

        [Test]
        public void TestSubstract()
        {
            int substractResult = 0;
            foreach (IntValuesResult ivr in this._substractValues)
            {
                substractResult = Calculator.substract(ivr.value1, ivr.value2);
                Assert.AreEqual(ivr.result, substractResult, "Values does not match!");
            }
        }

        [Test]
        public void TestMultiply()
        {
            int multiplyResult = 0;
            foreach (IntValuesResult ivr in this._multiplyValues)
            {
                multiplyResult = Calculator.multiply(ivr.value1, ivr.value2);
                Assert.AreEqual(ivr.result, multiplyResult, "Values does not match!");
            }
        }

        [Test]
        public void TestIntegerDivide()
        {
            int divideResult = 0;
            foreach (IntValuesResult ivr in this._divideValues)
            {
                divideResult = Calculator.integerDivision(ivr.value1, ivr.value2);
                Assert.AreEqual(ivr.result, divideResult, "Values does not match!");
            }
        }

        [TearDown]
        public void Teardown()
        {
            this._addValues = null;
            this._substractValues = null;
        }
    }
}