﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestingPipeline.Tests
{
    class IntValuesResult
    {
        public int value1;
        public int value2;
        public int result;

        public IntValuesResult(int v1, int v2, int r)
        {
            this.value1 = v1;
            this.value2 = v2;
            this.result = r;
        }
    }
}
